package com.sofco.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransaksiRequestDto {

    @JsonProperty("product_id")
    private Long productId;

    private int count;

    private String desc;

    private String name;

    private String address;
}
