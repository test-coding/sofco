package com.sofco.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransaksiResponseDto {

    private ProductResponseDto product;

    private int count;

    @JsonProperty("invoice_id")
    private String invoiceId;

    @JsonProperty("jumlah_bayar")
    private String jumlahBayar;

    @JsonProperty("nama_pelanggan")
    private String name;

    @JsonProperty("alamat_pelanggan")
    private String address;

    @JsonProperty("catatan_pelanggan")
    private String description;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", locale = "id-ID", timezone = "Asia/Jakarta")
    @JsonProperty("tanggal_pesan")
    private Date createdDate;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", locale = "id-ID", timezone = "Asia/Jakarta")
    @JsonProperty("tanggal_ubah")
    private Date lastUpdated;
}
