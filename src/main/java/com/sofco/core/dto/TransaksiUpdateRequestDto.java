package com.sofco.core.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransaksiUpdateRequestDto {

    @JsonProperty("invoice_id")
    private String invoiceId;

    private int count;

    private String desc;

    private String address;
}
