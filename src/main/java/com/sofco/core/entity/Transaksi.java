package com.sofco.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "transaksi", schema = "public")
public class Transaksi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "invoice_id")
    private String invoiceId;

    @OneToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumns({
            @JoinColumn(name = "product_id")
    })
    private Product product;

    private int count;

    private BigDecimal price;

    @Column(name = "count_price")
    private BigDecimal countPrice;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", locale = "id-ID", timezone = "Asia/Jakarta")
    @Column(name = "created_date")
    private Date createdDate;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", locale = "id-ID", timezone = "Asia/Jakarta")
    @Column(name = "last_updated")
    private Date lastUpdated;

    @Column(name = "description")
    private String description;

    private String name;

    private String address;

}
