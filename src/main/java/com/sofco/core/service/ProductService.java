package com.sofco.core.service;

import com.sofco.core.dto.ProductResponseDto;
import com.sofco.core.entity.Product;
import com.sofco.core.repository.ProductRepository;
import com.sofco.core.response.BaseResponse;
import com.sofco.core.response.ResponseBuilder;
import com.sofco.core.utils.Helper;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ModelMapper modelMapper;

    public BaseResponse<Page<ProductResponseDto>> getProduct(Pageable pageable) {
        final long start = System.nanoTime();

        Page<ProductResponseDto> response = productRepository.findAll(pageable).map(this::convertResponse);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse("00", ((end - start) / 1000000), "Success", response);
    }

    private ProductResponseDto convertResponse(Product product) {
        ProductResponseDto productResponseDto = modelMapper.map(product, ProductResponseDto.class);
        productResponseDto.setPrice(Helper.formatUang(Helper.objectToDouble(product.getPrice()), "#,###,###"));
        return productResponseDto;
    }
}
