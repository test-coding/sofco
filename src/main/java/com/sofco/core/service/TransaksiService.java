package com.sofco.core.service;

import com.sofco.core.dto.ProductResponseDto;
import com.sofco.core.dto.TransaksiRequestDto;
import com.sofco.core.dto.TransaksiResponseDto;
import com.sofco.core.dto.TransaksiUpdateRequestDto;
import com.sofco.core.entity.Product;
import com.sofco.core.entity.Transaksi;
import com.sofco.core.repository.ProductRepository;
import com.sofco.core.repository.TransaksiRepository;
import com.sofco.core.response.BaseResponse;
import com.sofco.core.response.ResponseBuilder;
import com.sofco.core.utils.Helper;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class TransaksiService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TransaksiRepository transaksiRepository;
    @Autowired
    private ModelMapper modelMapper;

    public BaseResponse<TransaksiResponseDto> inquiry(TransaksiRequestDto requestDto) {
        final long start = System.nanoTime();
        BigDecimal jumlahBayar = new BigDecimal(0);

        Product product = productRepository.findById(requestDto.getProductId()).orElse(null);
        if (null == product) {
            final long end = System.nanoTime();
            return ResponseBuilder.buildResponse("01", ((end - start) / 1000000), "Gagal", "Product ".concat(Helper.objectToString(requestDto.getProductId())).concat(" Not Found"));
        }

        jumlahBayar = product.getPrice().multiply(Helper.objectToBigDecimal(requestDto.getCount()));

        Transaksi transaksi = Transaksi.builder()
                .product(product)
                .name(requestDto.getName())
                .address(requestDto.getAddress())
                .description(requestDto.getDesc())
                .count(requestDto.getCount())
                .createdDate(new Date())
                .countPrice(jumlahBayar)
                .price(product.getPrice())
                .invoiceId(Helper.generateInvoiceId(transaksiRepository.seqInvoiceId()))
                .build();
        transaksi = transaksiRepository.save(transaksi);

        TransaksiResponseDto responseDto = convertResponse(transaksi);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse("00", ((end - start) / 1000000), "Pesanan telah diterima dengan nomor register ".concat(responseDto.getInvoiceId()), responseDto);
    }

    public BaseResponse<List<TransaksiResponseDto>> order(String namaPelanggan) {
        final long start = System.nanoTime();

        List<TransaksiResponseDto> myTransaksi = transaksiRepository.findAllByNameIgnoreCaseOrderByCreatedDateDesc(namaPelanggan).stream().map(this::convertResponse).collect(Collectors.toList());

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse("00", ((end - start) / 1000000), "Berhasil mendapatkan data pesanan", myTransaksi);
    }

    public BaseResponse<TransaksiResponseDto> orderUpdate(TransaksiUpdateRequestDto requestDto) {
        final long start = System.nanoTime();

        Transaksi trxUpdate = transaksiRepository.findAllByInvoiceId(requestDto.getInvoiceId());
        if (null == trxUpdate) {
            final long end = System.nanoTime();
            return ResponseBuilder.buildResponse("01", ((end - start) / 1000000), "Gagal", "Transaksi ".concat(Helper.objectToString(requestDto.getInvoiceId())).concat(" Not Found"));
        }

        trxUpdate.setCount(requestDto.getCount());
        trxUpdate.setCountPrice(trxUpdate.getPrice().multiply(Helper.objectToBigDecimal(requestDto.getCount())));
        if(null != requestDto.getAddress() && !"".equalsIgnoreCase(requestDto.getAddress())){
            trxUpdate.setAddress(requestDto.getAddress());
        }
        if(null != requestDto.getDesc() && !"".equalsIgnoreCase(requestDto.getDesc())){
            trxUpdate.setDescription(requestDto.getDesc());
        }

        trxUpdate.setLastUpdated(new Date());
        trxUpdate = transaksiRepository.save(trxUpdate);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse("00", ((end - start) / 1000000), "Berhasil ubah data pesanan", convertResponse(trxUpdate));
    }

    private ProductResponseDto convertResponse(Product product) {
        ProductResponseDto productResponseDto = modelMapper.map(product, ProductResponseDto.class);
        productResponseDto.setPrice(Helper.formatUang(Helper.objectToDouble(product.getPrice()), "#,###,###"));
        return productResponseDto;
    }

    private TransaksiResponseDto convertResponse(Transaksi transaksi) {
        TransaksiResponseDto responseDto = modelMapper.map(transaksi, TransaksiResponseDto.class);
        responseDto.setProduct(convertResponse(transaksi.getProduct()));
        responseDto.setJumlahBayar(Helper.formatUang(Helper.objectToDouble(transaksi.getCountPrice()), "#,###,###"));

        return responseDto;
    }
}
