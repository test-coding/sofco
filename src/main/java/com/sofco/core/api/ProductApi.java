package com.sofco.core.api;

import com.sofco.core.dto.ProductResponseDto;
import com.sofco.core.response.BaseResponse;
import com.sofco.core.service.ProductService;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping("/v1/api/product")
public class ProductApi {

    @Autowired
    private ProductService productService;

    @GetMapping()
    public ResponseEntity<BaseResponse<Page<ProductResponseDto>>> product(
            @ApiParam("pageNo") @RequestParam(required = false, defaultValue = "0") Integer pageNo,
            @ApiParam("Size data per page") @RequestParam(required = false, defaultValue = "3") Integer size
    ) {
        Pageable pageable = PageRequest.of(pageNo, size);
        return ResponseEntity.ok(productService.getProduct(pageable));
    }
}