package com.sofco.core.api;

import com.sofco.core.dto.TransaksiRequestDto;
import com.sofco.core.dto.TransaksiResponseDto;
import com.sofco.core.dto.TransaksiUpdateRequestDto;
import com.sofco.core.response.BaseResponse;
import com.sofco.core.service.TransaksiService;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/v1/api/transaksi")
public class TransaksiApi {

    @Autowired
    private TransaksiService transaksiService;

    @PostMapping("inquiry")
    public ResponseEntity<BaseResponse<TransaksiResponseDto>> transaksi(@RequestBody TransaksiRequestDto requestDto) {
        return ResponseEntity.ok(transaksiService.inquiry(requestDto));
    }

    @GetMapping("order")
    public ResponseEntity<BaseResponse<List<TransaksiResponseDto>>> order(
            @ApiParam("nama pelanggan") @RequestParam(required = false, defaultValue = "") String namaPelanggan
    ) {
        return ResponseEntity.ok(transaksiService.order(namaPelanggan));
    }

    @PutMapping("inquiry")
    public ResponseEntity<BaseResponse<TransaksiResponseDto>> order(
            @RequestBody TransaksiUpdateRequestDto requestDto
    ) {
        return ResponseEntity.ok(transaksiService.orderUpdate(requestDto));
    }

}