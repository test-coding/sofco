package com.sofco.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SofcoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SofcoApplication.class, args);
	}

}
