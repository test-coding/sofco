package com.sofco.core.response;

import java.time.LocalDateTime;

public class ResponseBuilder<T> {
    public static <T> T buildResponse(String code, long took, String message, Object data) {
        return (T) BaseResponse.builder()
                .code(code)
                .data(data)
                .message(message)
                .timeStamp(LocalDateTime.now())
                .took(took)
                .build();
    }
}
