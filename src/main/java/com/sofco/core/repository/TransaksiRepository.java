package com.sofco.core.repository;

import com.sofco.core.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransaksiRepository extends JpaRepository<Transaksi, Long> {
    @Query(value = "SELECT nextval('seq_transaksi')", nativeQuery = true)
    public Long seqInvoiceId();

    public List<Transaksi> findAllByNameIgnoreCaseOrderByCreatedDateDesc(String name);

    public Transaksi findAllByInvoiceId(String invoiceId);
}
