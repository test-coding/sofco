package com.sofco.core.utils;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Helper {
	private static Helper theInstance = new Helper();

	
	public Helper() {}
	
	public static Helper getInstance() {
		return theInstance;
	}
	
	public static String intToString(int param) {
		return "" + param;
	}
	
	public static int StringToInt(String param) {
		try {
			return Integer.parseInt(param);
		}catch (Exception e) {
			return 0;
		}
	}
	
	public static int LongToInt(Long param) {
		try {
			return param.intValue();
		}catch (Exception e) {
			return 0;
		}
	}
	
	public static long StringToLong(String param) {
		try {
			return Long.parseLong(param);
		}catch (Exception e) {
			return 0L;
		}
	}
	
//	public static SimResponse response(String rc, String rd) {
//		SimResponse response = new SimResponse();
//
//		response.setRc(rc);
//		response.setRd(rd);
//		response.setTimeStamp(System.currentTimeMillis());
//
//		return response;
//	}
	
	public static Long strToLong(String val) {
		Long result = 0L;
		try
	    {
			result = Long.parseLong(val);
	    } catch (Exception ex)
	    {
	        result = 0L;
	    }
		return result;
	}
	
	public static boolean isInteger(String param) {
		try
	    {
	        Integer.parseInt(param);
	        return true;
	    } catch (NumberFormatException ex)
	    {
	        return false;
	    }
	}
	
	public static Double objectToDouble(Object object) {
		Double result = 0.000;
		try{
			result = Double.parseDouble(object.toString());
		}catch (Exception e) {
			result = 0.00;
		}
		return result;
	}

	public static BigDecimal objectToBigDecimal(Object object) {
		BigDecimal result;
		try{
			result = new BigDecimal(object.toString());
		}catch (Exception e) {
			result = new BigDecimal(0);
		}
		return result;
	}
	
	public static long objectToLong(Object object) {
		long result;
		try{
			result = Long.parseLong(object.toString());
		}catch (Exception e) {
			result = 0;
		}
		return result;
	}

	public static int objectToInteger(Object object) {
		int result;
		try{
			result = Integer.parseInt(object.toString());
		}catch (Exception e) {
			result = 0;
		}
		return result;
	}
	
	public static String objectToString(Object object) {
		String result;
		try{
			result = object.toString();
		}catch (Exception e) {
			result = "";
		}
		if(object == null) {
			result = "";
		}
		return result;
	}

	public static String formatUang(double nilai, String str) {
		DecimalFormat myFormatter = new DecimalFormat(str, DecimalFormatSymbols.getInstance(getLocale()));
		return "Rp. " + myFormatter.format(nilai);
	}
	
	public static String formatUang3(double nilai, String str) {
		DecimalFormat myFormatter = new DecimalFormat(str, DecimalFormatSymbols.getInstance(getLocale()));
		return myFormatter.format(nilai);
	}
	
	public static Double formatUang2(double nilai, String str) {
		DecimalFormat myFormatter = new DecimalFormat(str, DecimalFormatSymbols.getInstance(getLocale()));
		return objectToDouble(myFormatter.format(nilai)) ;
	}
	
	public static Locale getLocale() {
		Locale local = Locale.getDefault();
		for (Locale l : Locale.getAvailableLocales()) {
			if (l.getLanguage().toUpperCase().equals("IN")) {
				local = l;
			}
		}
		return local;
	}
	public static String generateInvoiceId(Long idTransaksi) {
		String sId = String.format("%05d", idTransaksi);
		return "INV/SOFCO/".concat(sId);
	}
}
